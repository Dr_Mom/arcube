//
//  Bacardi.swift
//  ARKitExample
//
//  Created by Andrei Momot on 10/4/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

class Bacardi: VirtualObject {
    
    override init() {
        super.init(modelName: "bacardi", fileExtension: "scn", thumbImageFilename: "bottle", title: "Bacardi")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
